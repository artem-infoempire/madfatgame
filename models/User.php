<?php

//Class User - model for working with the users

Class User
{

    /**
     * Checking if there is a user in the database with such $email and $password
     */
    public static function checkUserData($email, $password)
    {
        $success = false;
        // Connecting Db
        $db = new Db;

        // Db query
        $sql = 'SELECT `id`, `password` FROM `user` WHERE `email` = :email';

        // Preparing statement
        $stmt = $db->connection->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        //Getting results
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($user) {
            // If there is a user - returning user password
            $storedPass = $user['password'];
            if(!empty($storedPass)){
                $success = Crypto::bcrypt_compare($password, $storedPass);
            }
            if($success) {
                self::auth($user['id']);
                return $user["id"];
            }
        }
        return false;
    }

    /**
     * Authenticating user
     */
    private static function auth($userId)
    {
        //Creating a unique token
        $sessionToken = Crypto::makeSalt(30);

        //Writing token to the DB
        $db = new Db;
        $sql = 'INSERT INTO `session` (`token`, `user_id`) values (:token, :userId);';

        // Preparing statement
        $stmt = $db->connection->prepare($sql);
        $stmt->bindParam(':token', $sessionToken);
        $stmt->bindParam(':userId', $userId);
        //If success - add token to the session
        if($stmt->execute()){
            $_SESSION["token"] = $sessionToken;
        }
    }

    // Logging user out
    public static function logout(){
        if(isset($_SESSION['token'])){
            //Removing session from DB
            $db = new Db;
            $sql = 'DELETE from `session` where `token`=:token;';

            $stmt = $db->connection->prepare($sql);
            $stmt->bindParam(':token', $_SESSION['token']);

            //if success - remove token from session
            if($stmt->execute()) {
                unset($_SESSION['token']);
            }
        }

        Header("Location: /");
    }

    /**
     * Getting user id if he is authorized
     */
    public static function checkLogged()
    {
        // If session exists - getting token and checking if user is authorized
        if (isset($_SESSION['token'])) {
            $token = $_SESSION['token'];
            return self::validateSession($token);
        }

        //else redirecting user to the homepage
        header("Location: /");
    }

    //Function checks whether a user is a guest
    public static function isGuest(){
        if (isset($_SESSION['token'])) {
            $id = false;
            $token = $_SESSION['token'];
            $id = self::validateSession($token);
            if($id){
                return false;
            }
        }
        return true;
    }

    private static function validateSession($token){
        $db = new Db;
        // Db query
        $sql = 'SELECT `user_id` FROM `session` WHERE `token` = :token';

        // Preparing statement
        $stmt = $db->connection->prepare($sql);
        $stmt->bindParam(':token', $token);
        $stmt->execute();

        //Getting results
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if($user){
            self::updateSession($token);
            return $user["user_id"];
        }
        return false;
    }

    //Function renews token update time
    private static function updateSession($token){
        $db = new Db;
        $updateTime = time();
        // Db query
        $sql = "Update `session` SET `updated` = FROM_UNIXTIME(:time) WHERE `token`= :token;";

        // Preparing statement
        $stmt = $db->connection->prepare($sql);
        $stmt->bindParam(':time', $updateTime);
        $stmt->bindParam(':token', $token);
        $stmt->execute();
    }

    /**
     * Function returns user info by id
     */
    public static function getUserById($id)
    {
        //Connecting DB
        $db = new Db();

        //Preparing Db Query
        $sql = 'SELECT t1.id, t1.email, t1.password, t2.name as type, t3.name as measure, t1.date_registered FROM user t1
INNER JOIN user_type t2 on t1.type_id = t2.id
INNER JOIN measure t3 on t1.measure_id = t3.id
WHERE t1.id = :id;';

        // Execuring query
        $stmt = $db->connection->prepare($sql);
        if(!$stmt){
            return false;
        }
        $stmt->bindParam(':id', $id);
        if(!$stmt->execute()){
            return false;
        }

        //fetch results
        $selection = $stmt->fetch(PDO::FETCH_ASSOC);

        return $selection;
    }

    /**
     * Function returns user profile info by user id
     */
    public static function getUserProfileById($id)
    {
        //Connecting DB
        $db = new Db();

        //Preparing Db Query
        $sql = 'SELECT t1.id, t1.user_id, t1.first_name, t1.last_name, t1.logo, t1.height, t1.weight, t1.birthday, t1.sex, t1.phone, t1.city, t1.facebook_url, t1.instagram_url, t2.name as country FROM user_profile t1
INNER JOIN country t2 on t1.country_id = t2.id
WHERE user_id=:id;';

        // Execuring query
        $stmt = $db->connection->prepare($sql);
        if(!$stmt){
            return false;
        }
        $stmt->bindParam(':id', $id);
        if(!$stmt->execute()){
            return false;
        }

        //fetch results
        $selection = $stmt->fetch(PDO::FETCH_ASSOC);

        return $selection;
    }

    //Functino verifies if the password is correct
    public static function checkPassword($id, $password){
        $success = false;
        //Connecting DB
        $db = new Db();

        //Preparing Db Query
        $sql = 'SELECT password FROM user WHERE id=:id;';

        // Execuring query
        $stmt = $db->connection->prepare($sql);
        if(!$stmt){
            return $success;
        }
        $stmt->bindParam(':id', $id);
        if(!$stmt->execute()){
            return $success;
        }

        //fetch results
        $selection = $stmt->fetch(PDO::FETCH_ASSOC);
        $storedPass = $selection["password"];
        if(!empty($storedPass)){
            $success = Crypto::bcrypt_compare($password, $storedPass);
        }
        return $success;
    }

    //Function updates the password
    public static function updatePassword($id, $password){
        $success = false;
        //encrypting the password
        $password = Crypto::bcrypt_generate($password);

        //Connecting DB
        $db = new Db();

        //Preparing Db Query
        $sql = 'Update user SET password=:password WHERE id=:id;';

        // Execuring query
        $stmt = $db->connection->prepare($sql);
        if(!$stmt){
            return $success;
        }
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':password', $password);
        if($stmt->execute()) {
            $success = true;
        }
        return $success;
    }
    
}