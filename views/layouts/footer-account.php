<!--Blazy image-->
<script type="text/javascript" src="/template/js/blazy.min.js"></script>

<!--Waypoint-->
<script src="/template/js/waypoints.min.js"></script>

<script src="/template/js/ios-orientationchange-fix.js"></script>

<script src="/template/plugins/bxslider/jquery.bxslider.min.js"></script>

<!--Sly Slider-->

<!--Bx Slider-->
<script src="/template/plugins/bxslider/jquery.bxslider.min.js"></script>
<!--Flex Slider-->
<script src="/template/plugins/flexslider/jquery.flexslider-min.js"></script>

<!-- Catalog Price-->
<script src="/template/plugins/nouislider/jquery.nouislider.min.js"></script>
<!-- Magnific-->
<script type="text/javascript" src="/template/plugins/magnific/jquery.magnific-popup.js"></script>
<!-- Pretty Photo -->
<script src="/template/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
<!-- SMart Menu -->
<script src="/template/plugins/smart-menu/smart-menu.js"></script>

<script type="text/javascript" src="/template/plugins/sly/sly.min.js"></script>
<!-- Loader -->
<script src="/template/plugins/loader/js/classie.js"></script>
<script src="/template/plugins/loader/js/pathLoader.js"></script>
<script src="/template/plugins/loader/js/main.js"></script>
<script type="text/javascript" src="/template/js/cssua.min.js"></script>
<script src="/template/plugins/points/points.js" ></script>
<script src="/template/plugins/events/events.js"></script>

<!--<script src="plugins/selectbox/jquery.selectbox-0.2.js"></script> 


<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script>-->


<script src="/template/js/jqBootstrapValidation.js"></script>
<script src="/template/js/contact_me.js"></script>
<script src="/template/js/jquery.validate.js"></script>
<script src="/template/plugins/events/masonry.pkgd.min.js"></script>


<script src="/template/plugins/newsletter/main.js" ></script>
<script type="text/javascript" src="/template/plugins/bio/js/bio.js"></script>


<script src="/template/plugins/countdown/jquery.countdown.min.js" ></script>



<!--IVIEW-->
<script src="/template/plugins/iview/js/iview.js"></script>

<!--Scripts-->
<script type="text/javascript" src="/template/js/scripts.js"></script>
<script type="text/javascript" src="/template/js/account.js"></script>

</body>
</html>
