<?php
include_once ROOT . "/views/layouts/header-account.php";
?>

<section id="account">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div>
                    <img src="/template/img/madfatgame-logo.png" alt="#madfatgame" class="madfat-logo"/>
                    <h3 class="text-center"><?php echo $userProfile["first_name"] . " " . $userProfile["last_name"]; ?></h3>
                </div>
                <div class="list-group">
                    <a href="/account/profile" class="list-group-item">Profile</a>
                    <a href="/account/anthropometry" class="list-group-item active">Anthropometry</a>
                    <a href="/account/photos" class="list-group-item">Photos</a>
                    <a href="/account/challenges" class="list-group-item">Challenges</a>
                    <a href="/account/instastars" class="list-group-item">Instastars</a>
                    <a href="/account/chat" class="list-group-item">Chat</a>
                    <a href="/account/logout" class="list-group-item">Logout</a>
                </div>
            </div>
            <div class="col-sm-10">



            </div>
        </div>
    </div>
</section>

<?php include_once ROOT . "/views/layouts/footer-account.php"; ?>
