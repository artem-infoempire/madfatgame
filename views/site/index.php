<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<title>#MadFatGame</title>

<link rel="shortcut icon" href="http://infoempire.us/madfatgame/img/favicon.png" type="image/png" />
<link rel="icon" href="http://infoempire.us/madfatgame/img/favicon.png" type="image/png" />
<!-- Master CSS -->
<link href="/template/css/master.css" rel="stylesheet">
<link href="/template/css/owl.carousel2.css" rel="stylesheet">
<link href="/template/css/owl.transitions.css" rel="stylesheet">
<link rel="stylesheet" href="/template/css/dropdown.css">

<!-- jQuery-->
<script src="/template/js/jquery-1.11.1.min.js"></script>
<script src="/template/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/template/js/jquery-ui.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/modernizr.js"></script>
<script src="https://use.fontawesome.com/ef87f083a6.js"></script>
<script src="/template/js/dropdown.js"></script>
<script src="/template/js/owl.carousel2.min.js"></script>


</head>

<!--<body class="animated-all">-->
<body>

<!-- Loader Landing Page -->
<div id="ip-container" class="ip-container"> 
  <!-- initial header -->
  <header class="ip-header">
    <div class="col-lg-12 text-center">
      <div class="ip-logo"><img  src="/template/img/logo-loader.jpg" alt="madfatgame logo loader" width="230" height="300" class="loading-logo"></div>
    </div>
    <div class="ip-loader"> <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
      <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,39.3,10z"/>
      <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
      </svg> </div>
  </header>
</div>
<!-- Loader end -->


<div class="header header-home" style="position:relative">

<img src="/template/img/slide-1.jpg" width="1920" height="960" style="width:100%; height:auto">


<!--<div class="navbar yamm navbar-default ">
    <div class="container">
      <nav id="navbar-collapse-1" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="#prizes" class="hvr-grow-rotate page-scroll">Prizes</a></li>
          <li><a href="#rules" class="hvr-grow-rotate page-scroll">Rules</a></li>
          
          <li><a href="#faq" class="hvr-grow-rotate page-scroll">FAQ</a></li>
          <li><a href="#contact" class="hvr-grow-rotate page-scroll">Contact</a></li>
          <li class="social">
              <a href="https://www.instagram.com/madfatgame/" target="_blank" class="hvr-grow-rotate"><i class="fa fa-instagram"></i></a>
              <a href="https://www.facebook.com/madfatgame/" target="_blank" class="hvr-grow-rotate"><i class="fa fa-facebook-square"></i></a>
          </li>
            
            <?php
                if(User::isGuest()){
            ?>
                <li class="signin"><a href="#signin" class="hvr-grow-rotate" data-toggle="modal" data-target="#signin">Sign in</a></li>
            <?php
                }
                else{
            ?>
                    <li class="signin"><a href="/account/logout" class="hvr-grow-rotate">Sign Out</a></li>
                    <li class="signin"><a href="/account/profile" class="hvr-grow-rotate">My Account</a></li>
            <?php
                }
            ?>
            
         <li class="signin"><a href="#getticket" class="hvr-grow-rotate page-scroll">Get Ticket</a></li>
        </ul>
      </nav>
    </div>
  </div>-->
  
</div>


    <div class="nav" id="navigationbar">
        <div class="container">
            <div class="row">
                <div class="twelve columns">
                    <div id="cssmenu">
                        <ul class="nav navbar-nav">
                            <li><a href="#prizes" class="hvr-grow-rotate page-scroll">Prizes</a></li>
                            <li><a href="#rules" class="hvr-grow-rotate page-scroll">Rules</a></li>

                            <li><a href="#faq" class="hvr-grow-rotate page-scroll">FAQ</a></li>
                            <li><a href="#contact" class="hvr-grow-rotate page-scroll">Contact</a></li>

                            <?php
                            if(User::isGuest()){
                                ?>
                                <li class="signin"><a href="#signin" class="hvr-grow-rotate" data-toggle="modal" data-target="#signin">Sign in</a></li>
                                <?php
                            }
                            else{
                                ?>
                                <li class="signin"><a href="/account/logout" class="hvr-grow-rotate">Sign Out</a></li>
                                <li class="signin"><a href="/account/profile" class="hvr-grow-rotate">My Account</a></li>
                                <?php
                            }
                            ?>

                            <li class="signin"><a href="#getticket" class="hvr-grow-rotate page-scroll">Get Ticket</a></li>

                            <li class="social">
                                <a href="https://www.instagram.com/madfatgame/" target="_blank" class="hvr-grow-rotate"><i class="fa fa-instagram"></i></a>
                                <a href="https://www.facebook.com/madfatgame/" target="_blank" class="hvr-grow-rotate"><i class="fa fa-facebook-square"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- SECTION -->

<section class="no-bg-color-parallax parallax-black  home-section"  >
<div class="section-bg" style="background-image:url(/template/img/season-starts.jpg)"></div>

 
  <div class="container">
    <div class="row">
      <div class="animated" data-animation="bounceInUp">

            <div class=" col-sm-12 col-md-12 col-lg-12"  >
              <div class="x-coutdown">
                <h2 class="section-heading white">Season 1<br>
Starts June 1, 2017</h2>
                <div class="x-coutdown">
                  <div id="x-coutdown-timer" data-countdown="2017/06/01"></div>
                </div>
              </div>
            </div>
         
        
        
         <script>
    
   $(document).ready(function(){ 


     /////////////////////////////////////
    //  Clock
    /////////////////////////////////////
		
$('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   $this.countdown(finalDate, function(event) {

	 $this.html(event.strftime(''
     + '<div class="x-cdr"><span>%D</span> <strong>day%!d</strong> </div>'
     + '<div class="x-cdr"><span>%H</span> <strong>hr</strong> </div>'
     + '<div class="x-cdr"><span>%M</span> <strong>min</strong> </div>'
     + '<div class="x-cdr last"><span>%S</span> <strong>sec</strong> </div>'));
 
	 
   });
 });

});

   </script> 
   
   
   
        
      </div>
    </div>
    </div>
    </section>


<section  class="no-bg-color-parallax home-section" style="padding:0;">
<img src="/template/img/slide-2.jpg"  alt="fat wanted" class="img-responsive">

 </section>

<!-- SECTION -->

<section class="home-section  parallax-black" id="prizes">

<ul class="bg-slideshow">
    <li>
      <div class="bg-slide" style="background-image:url(/template/img/bg-prizes.jpg)"></div>
    </li>
    <li>
      <div class="bg-slide" style="background-image:url(/template/img/bg-prizes.jpg)"></div>
    </li>
  </ul>
  
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="heading-wrap animated"  data-animation="bounceInUp"  >
        
          <h2 class="section-heading white">Lose weight - get paid</h2>
         <h3 class="section-subheading hang">Top prizes for best players</h3>
        </div>
      </div>
    </div>
    </div>
    <div  class="container">
      <div class="row">
        <div class="col-md-4 text-center ft-box hidden-sm hidden-xs"> 
        <img src="/template/img/iphone.png" width="570" height="670" alt="iPhone 7" class="img-responsive">
       <h4>iPhone 7</h4>
        </div>
         <div class="col-sm-4 text-center ft-box" > 
         <img src="/template/img/cash.png" width="570" height="670" alt="1000 cash" class="img-responsive">
<h4>$1000</h4>
        </div>
         <div class="col-sm-4 text-center ft-box hidden-lg hidden-md "> 
        <img src="/template/img/iphone.png" width="570" height="670" alt="iphone" class="img-responsive">
       <h4>iPhone 7</h4>
        </div>
         <div class="col-sm-4 text-center ft-box"> 
         <img src="/template/img/gift-cert-girl.png" width="570" height="670" alt="gift cert" class="img-responsive">
       <h4>$400 Lingerie Gift Certificate</h4>
        </div>
        
      </div>
      <div class="section-footer sf-type-2">
      <div class="sf-left" style="background-color:#C09"></div>
      <div class="sf-right"  style="background-color:gold" ></div>
    </div>
    </div>
</section>



<section  class="no-bg-color-parallax home-section  parallax-black">
 <div class="section-bg" style="background-image:url(/template/img/bg-instastars.jpg)"></div>
 <div class="container" >
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="heading-wrap animated"  data-animation="bounceInUp"  >
     
          <h2 class="section-heading white big">Next 7 champions<br>
win $100
</h2>
         
        </div>
      </div>
    </div>
    </div>

  <div class="section-footer sf-type-2">
      <div class="sf-left" style="background-color:yellow"></div>
      <div class="sf-right" style="background-color:red" ></div>
    </div>
 </section>




<!-- SECTION -->

<section class="home-section parallax-white ">
<ul class="bg-slideshow">
    <li>
      <div class="bg-slide" style="background-image:url(/template/img/bg-instastars.jpg)"></div>
    </li>
    <li>
      <div class="bg-slide" style="background-image:url(/template/img/bg-instastars.jpg)"></div>
    </li>
  </ul>

  
  
    <div class="container">
    <div class="row">
      <div class="col-lg-12 ">
        <div class="heading-wrap animated"  data-animation="bounceInUp"  >
         <div class="small-logo"><a href="https://www.instagram.com/madfatgame/" target="_blank" class="hvr-grow"><img width="300" height="70" alt="#madfatgame" src="/template/img/hashtag-madfatgame.png"  style="float:left;"></a> <span><img src="/template/img/season-1.png" width="190" height="70" alt="Season 1"></span></div>
          <h2 class="section-heading big"><span class="hash"><i class="fa fa-instagram"></i></span> InstaStars</h2>
          <div class="text-center"><h3 class="section-subheading hang">Use #madfatgame hashtag to let your InstaWorld know that you are IN the Game, and win Bonus Prizes</h3></div>
        </div>
      </div>
    </div>
    </div>
  <div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="owl-carousel owl-theme" id="instaprices">
                 <div class="text-center ft-box hvr-bob">
                   <img src="/template/img/bike.png" alt="Bikes" class="img-responsive">
                      <h4>Bikes</h4>
                  </div>

                  <div class="text-center ft-box hvr-bob">
                      <img src="/template/img/pushup-bar.png" alt="Push-up bars" class="img-responsive">
                      <h4>Push-up bars</h4>
                  </div>

                  <div class="text-center ft-box hvr-bob">
                      <img src="/template/img/tablet.png" alt="Tablets" class="img-responsive">
                      <h4>Tablets</h4>
                  </div>

                  <div class="text-center ft-box hvr-bob">
                      <img src="/template/img/gyroscooter.png" alt="Gyroscooters" class="img-responsive">
                      <h4>Gyroscooters</h4>
                  </div>

                  <div class="text-center ft-box hvr-bob">
                      <img src="/template/img/nutrition.png" alt="Sport Nutriotions" class="img-responsive">
                      <h4>Sport Nutriotions</h4>
                  </div>

                  <div class="text-center ft-box hvr-bob">
                   <img src="/template/img/gift-card.png" alt="Gift cards" class="img-responsive">
                      <h4>Gift cards</h4>
                  </div>

                  <div class="text-center ft-box hvr-bob">

                   <img src="/template/img/ipod.png" alt="iPods" class="img-responsive">
                      <h4>iPods</h4>

                  </div>

                  <div class="text-center ft-box hvr-bob">
                   <img src="/template/img/beats-pill.png" alt="Beats Pill" class="img-responsive">
                      <h4>Beats Pills</h4>
                  </div>

                  <div class="text-center ft-box hvr-bob">
                   <img src="/template/img/wearable.png" alt="Fitness tracker" class="img-responsive">
                      <h4>Fitness trackers</h4>
                  </div>
            </div>
        </div>
    </div>
  </div>
</section>
<img src="/template/img/main-prize.jpg" width="1280" height="720" class="img-responsive main-prize">
<!--<section  class="no-bg-color-parallax  home-section">
 <div class="section-bg" style="background-image:url(img/main-prize.jpg)"></div>
 <div class="container" style="min-height:400px;">
    <div class="row">
      <div class="col-lg-12">
        <div class="heading-wrap animated"  data-animation="bounceInUp"  >
         
       <h2 class="section-heading white big" style="text-align:left"><br>
<br>
 The main prize is<br>
Your NEW SEXY BODY</h2>
         
        </div>
      </div>
    </div>
    </div>-->

 <!-- <div class="section-footer sf-type-2">
      <div class="sf-left" style="background-color:#C09"></div>
      <div class="sf-right" style="background-color:gold" ></div>
    </div>-->


<!-- SECTION --> 

<!-- SECTION -->

<section  class="no-bg-color-parallax  home-section" id="rules" >
 <div class="section-bg" style="background-image:url(/template/img/bg-prizes.jpg)"></div>
 
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="heading-wrap animated"  data-animation="bounceInUp"  >
        
          <h2 class="section-heading white big">Rules</h2>
         
        </div>
      </div>
    </div>
    </div>
    
    <div id="steps" class="animated"  data-animation="bounceInUp">
    <div class="col-md-2 col-sm-4 col-xs-12 text-center step hvr-bounce-in"><span>1</span><img src="/template/img/icon-ticket.png" width="100" height="100" alt="buy a ticket"><h4>Buy a ticket</h4></div>
    <div class="col-md-2 col-sm-4 col-xs-12 text-center step hvr-bounce-in"><span>2</span><img src="/template/img/icon-before.png" width="100" height="100" alt="Make a photo BEFORE"><h4>Make a photo BEFORE</h4></div>
    <div class="col-md-2 col-sm-4 col-xs-12 text-center step hvr-bounce-in"><span>3</span><img src="/template/img/icon-practice.png" width="100" height="100" alt="Make a photo BEFORE"><h4>Work out &amp; eat healthy for 1 month</h4></div>
    <div class="col-md-2 col-sm-4 col-xs-12 text-center step hvr-bounce-in"><span>4</span><img src="/template/img/icon-after.png" width="100" height="100" alt="Make a photo AFTER"><h4>Make a photo AFTER</h4></div>
    <div class="col-md-2 col-sm-4 col-xs-12 text-center step hvr-bounce-in"><span>5</span><img src="/template/img/icon-like.png" width="100" height="100" alt="Make a photo BEFORE"><h4>Get LIKES</h4></div>
    <div class="col-md-2 col-sm-4 col-xs-12 text-center step hvr-bounce-in last"><span>6</span><img src="/template/img/icon-win.png" width="100" height="100" alt="Make a photo BEFORE"><h4>WIN!</h4></div>
    <br clear="all">
    </div>
  <div class="container" style="margin-top:40px;">
    <div class="row">
      <div class="col-md-12 text-center " >
        <div class="animated"   data-animation="bounceInUp">
       
          <h2 class="section-heading white big">Where is the catch?</h2>
          <p class="white" style="font-size:22px;">You will need to jump through some hoops.<br>
Every day I send you exercising tasks and meal plan, you can follow it and WIN or IGNORE and FAIL.</p>

<p style="font-size:22px; color:#fff; max-width:695px; padding:0 40px; position:relative; margin:0 auto 40px auto; text-transform:uppercase"><span style="position:absolute; font-size:140px; left:0; top:0; line-height:0.8">!</span>
At the end of each week, you have to PASS an EXAM. Losers will be <strong>ELIMINATED</strong>.<br>
Prove you are not a candy ass move on and win cash!<span style="position:absolute; font-size:140px; right:0; top:0; line-height:0.8">!</span></p>
        </div>
      </div>
      
      
    </div>
  </div>
  


 

  <div class="section-footer sf-type-2">
     <div class="sf-left" style="background-color:#F06"></div>
     <div class="sf-right" style="background-color:#F90"></div>
    </div>
    
</section>

<section  class="no-bg-color-parallax parallax-black  home-section">
 <div class="section-bg" style="background-image:url(/template/img/bg-prizes.jpg)"></div>
<div class="container" style="min-height:400px;">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="heading-wrap animated"  data-animation="bounceInUp"  >
         
          <h2 class="section-heading white big">Who am I?</h2>
     <div class="video-wrapper">  
     <div class="video-player">
<div class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/08u_Gvgwrfs?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
</div>
</div>
         
        </div>
      </div>
    </div>
    </div>

</section>


<!-- SECTION --> 



<!-- SECTION -->


<section  class="home-section" id="getticket" style=" background:url(/template/img/get-ticket.jpg) top center no-repeat #fff;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="heading-wrap animated"  data-animation="bounceInUp" >
          <div class="small-logo"><a href="https://www.instagram.com/madfatgame/" target="_blank" class="hvr-grow"><img width="300" height="70" alt="#madfatgame" src="/template/img/hashtag-madfatgame.png"  style="float:left;"></a> <span><img src="/template/img/season-1.png" width="190" height="70" alt="Season 1"></span></div>
          <h2 class="section-heading">Get Ticket</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
      <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

              <!-- PRICE ITEM -->
              <div class="panel price panel-purple">
                  <div class="panel-heading  text-center">
                      <h3>Single</h3>
                  </div>
                  <div class="panel-body text-center">
                      <h2 class="lead">Rocky</h2>
                  </div>
                  <div class="desc text-center">
                      <h3>You can get in shape and make a pretty penny</h3>
                      <div class="amount">$60</div>
                  </div>
                  <div class="panel-footer">
                      <a class="btn btn-lg btn-block" href="#">BUY NOW</a>
                  </div>
              </div>
              <!-- /PRICE ITEM -->


          </div>

          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

              <!-- PRICE ITEM -->
              <div class="panel price panel-pink">
                  <div class="panel-heading arrow_box text-center">
                      <h3>1+1</h3>
                  </div>
                  <div class="panel-body text-center">
                      <h2 class="lead">Mr. &amp; Ms. Smith</h2>
                  </div>
                  <div class="desc text-center">
                      <h3> It is a plan for you and your partner/lover/friend</h3>
                      <div class="amount">$100</div>
                  </div>
                  <div class="panel-footer">
                      <a class="btn btn-lg btn-block" href="#">BUY NOW</a>
                  </div>
              </div>
              <!-- /PRICE ITEM -->


          </div>

          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

              <!-- PRICE ITEM -->
              <div class="panel price panel-red">
                  <div class="panel-heading arrow_box text-center">
                      <h3>VIP</h3>
                  </div>
                  <div class="panel-body text-center">
                      <h2 class="lead">The Godfather</h2>
                  </div>
                  <div class="desc text-center">
                      <h3>Pay and go directly into finals. Do whatever you want, I do not care</h3>
                      <div class="amount">$200</div>
                  </div>
                  <div class="panel-footer">

                      <a class="btn btn-lg btn-block" href="#">BUY NOW</a>
                  </div>
              </div>
              <!-- /PRICE ITEM -->


          </div>



      </div>
  </div>
</section>


<!-- SECTION -->

<section class="no-bg-color-parallax parallax-black home-section" id="faq" >
 <div class="section-bg" style="background-image:url(/template/img/bg-flares.jpg)"></div>
 
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="heading-wrap animated"  data-animation="bounceInUp" >
          <h2 class="section-heading white">FAQ</h2>
        </div>
      </div>
    </div>
    <div class="col-lg-12 animated"  data-animation="bounceInUp">
      <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">TIMING</a></li>
                <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">EXERCISING</a></li>
                <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">LIKES &amp; PRIZES</a></li>
                <li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">ABOUT KIDS</a></li>
                <li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">ABOUT HEALTH</a></li>
                <li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">MR. MAD</a></li>
                <li role="presentation"><a href="#tab7" role="tab" data-toggle="tab">FREE TICKET</a></li>
              </ul>
              
              
<div class="tab-content">

<!-- TAB 1 -->
<div role="tabpanel" class="tab-pane active" id="tab1">

 				<div id="accordion" class="panel-group">
                
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1-1">When does game start?</a></h4>
                          </div>
                          <div id="collapse1-1" class="panel-collapse collapse in">
                            <div class="panel-body">The game starts July 1, 9:00pm, New York Time Zone (Eastern Standard Time). Follow Instagram <a href="https://www.instagram.com/madfatgame/" target="_blank">@madfatgame</a> for details.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1-2">I have never tried any sports? Is this game for me? </a></h4>
                          </div>
                          <div id="collapse1-2" class="panel-collapse collapse">
                            <div class="panel-body">It's never too late to start.</div>
                        </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1-3">I have a full-time job. Do you think I can do it?</a></h4>
                          </div>
                          <div id="collapse1-3" class="panel-collapse collapse">
                            <div class="panel-body">Average workout time is 30-40 min. You can do it before/after or even during the work process and upload report into your account before the deadline. If you WANT, you definitely CAN play this game.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1-4">If I can not do exercises every day?</a></h4>
                          </div>
                          <div id="collapse1-4" class="panel-collapse collapse">
                            <div class="panel-body">Every week you have to pass an "EXAM". I post it on Friday 9pm (Eastern Standard Time) and you have 27 hours to complete the task and upload the video proof. You don't need to submit daily challenges. But if you don't, you potentially can FAIL an "EXAM". If you want to get in shape, lose weight and get a beach-ready body, I strongly suggest you submit all the reports.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1-5">How long does workout take?</a></h4>
                          </div>
                          <div id="collapse1-5" class="panel-collapse collapse">
                            <div class="panel-body">30-40 min</div>
                          </div>
                        </div>
                        
                       
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1-6">What is my Time Frame to submit a report? </a></h4>
                          </div>
                          <div id="collapse1-6" class="panel-collapse collapse">
                            <div class="panel-body">Until 11:59pm (Eastern Standard Time) the next day after the task is announced.</div>
                          </div>
                        </div>
                        
                         <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1-7">What if I live in a different Time Zone?</a></h4>
                          </div>
                          <div id="collapse1-7" class="panel-collapse collapse">
                            <div class="panel-body">You still have enough time to submit a report.</div>
                          </div>
                        </div>
                        
                        
                      </div>
                      
            </div>  



<!-- END TAB 1 -->

<!-- TAB 2 -->
<div role="tabpanel" class="tab-pane" id="tab2">

 				<div id="accordion2" class="panel-group">
                
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-1">Are exercising the same or different for men and women?</a></h4>
                          </div>
                          <div id="collapse2-1" class="panel-collapse collapse in">
                            <div class="panel-body">We support sexual equality. Men and women do the same exercises.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-1-1">What kind of exercising will I do ?</a></h4>
                          </div>
                          <div id="collapse2-1-1" class="panel-collapse collapse">
                            <div class="panel-body "><section class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/08u_Gvgwrfs?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></section></div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-2">Can I Test-Drive the Game and get a refund?</a></h4>
                          </div>
                          <div id="collapse2-2" class="panel-collapse collapse">
                            <div class="panel-body">No.</div>
                        </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-3">Who can be "ELIMINATED"? </a></h4>
                          </div>
                          <div id="collapse2-3" class="panel-collapse collapse">
                            <div class="panel-body">You can be eliminated if You:<br>
- Do not pass a weekly "EXAM" (Late to submit an "EXAM", incorrect exercise technique, weakest results)<br>
- Do not follow the task instructions<br>
- Allow inappropriate game behavior (For example: Illegal advertising of Good and Services, any abuse)
</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-4">Do I need to videotape all my exercising? </a></h4>
                          </div>
                          <div id="collapse2-4" class="panel-collapse collapse">
                            <div class="panel-body">NO. Only once a week when you do an "EXAM".</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-5">What if nobody can videotape me? </a></h4>
                          </div>
                          <div id="collapse2-5" class="panel-collapse collapse">
                            <div class="panel-body">Well. Be inventive. Use your brain. Place your phone on the table and use any support devices. Call your friends, a boyfriend, a neighbor or a grandma for help. They would be happy to see how you work a sweat.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-6">How do you check everyday tasks and weekly "EXAM"?</a></h4>
                          </div>
                          <div id="collapse2-6" class="panel-collapse collapse">
                            <div class="panel-body">Every day online Guru will read your reports and reply. Every Saturday online Guru will watch your "EXAM" video and let you know if you FAIL or PASS. If I catch you cheating you can win the main prize - BAN.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-7">I am not feeling comfortable to upload my photo and video to Social Networks.</a></h4>
                          </div>
                          <div id="collapse2-7" class="panel-collapse collapse">
                            <div class="panel-body">Instagram or Facebook uploads are not mandatory. Only you can decide to do it or not. However, I strongly encourage you to make your efforts public, because being Instagram active you can become #MadFatGame InstaStar and win prizes. But there is another reason as well: once you let all your friend know that you are IN, you have no turning back, otherwise they will call you fat-mouth until the end of your life. Not bad motivation, isn't it?</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-8">Who can see my "EXAM" video? </a></h4>
                          </div>
                          <div id="collapse2-8" class="panel-collapse collapse">
                            <div class="panel-body">Only online GURU.</div>
                          </div>
                        </div>
                        
                      </div>
            </div>  



<!-- END TAB 2 -->


<!-- TAB 3 -->
<div role="tabpanel" class="tab-pane" id="tab3">

 				<div id="accordion3" class="panel-group">
                
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#collapse3-1">How do you pick the winner? </a></h4>
                          </div>
                          <div id="collapse3-1" class="panel-collapse collapse in">
                            <div class="panel-body">Winner will be determined by the open vote.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#collapse3-2">How can I get my prize?</a></h4>
                          </div>
                          <div id="collapse3-2" class="panel-collapse collapse">
                            <div class="panel-body">Cash prize – Direct Deposit.  Other prizes will be shipped to your address.</div>
                        </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#collapse3-3">Can I trade my prize with the other winner? </a></h4>
                          </div>
                          <div id="collapse3-3" class="panel-collapse collapse">
                            <div class="panel-body">If you don't need your prize or already have the same, you can exchange it with other participants. This is between you and him/her. You survived, went through many hardships I think you can easy come to an arrangement.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#collapse3-4">Can I take cash instead of prize? </a></h4>
                          </div>
                          <div id="collapse3-4" class="panel-collapse collapse">
                            <div class="panel-body">No.<br>
With only one exception. If a man wins lingerie certificate, he has a choice to gift it to his girlfriend or to take cash.</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3" href="#collapse3-5">I live in Mexico, how can I get my prize? </a></h4>
                          </div>
                          <div id="collapse3-5" class="panel-collapse collapse">
                            <div class="panel-body">Please read above.
</div>
                          </div>
                        </div>
                        
                        
                      </div>
            </div>  



<!-- END TAB 3 -->

<!-- TAB 4 -->
<div role="tabpanel" class="tab-pane" id="tab4">

 				<div id="accordion4" class="panel-group">
                
                        
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion4" href="#collapse4-1">I am a breastfeeding mother, can I play your Fitness Game?</a></h4>
                          </div>
                          <div id="collapse4-1" class="panel-collapse collapse in">
                            <div class="panel-body">Yes, you can. Special food plan and exercises are safe for you. But I recommend you to consult with your family physician.</div>
                        </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion4" href="#collapse4-2">Do I have to go jogging? </a></h4>
                          </div>
                          <div id="collapse4-2" class="panel-collapse collapse">
                            <div class="panel-body">No, you don't have to. All exercises designed for in the house workout. You can replace jogging with skipping.
</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion4" href="#collapse4-3">I have 2 little kids and my husband works 24 hours. I can not leave my house.</a></h4>
                          </div>
                          <div id="collapse4-3" class="panel-collapse collapse ">
                            <div class="panel-body">You can stay home. Everyday exercises and "EXAMs" are designed for the in-house training.</div>
                          </div>
                        </div>
                        
                        
                      </div>
            </div>  


<!-- END TAB 4 -->

<!-- TAB 5 -->
<div role="tabpanel" class="tab-pane" id="tab5">

 				<div id="accordion5" class="panel-group">
                
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5" href="#collapse5-1">What if I have periods during the Fitness Game?</a></h4>
                          </div>
                          <div id="collapse5-1" class="panel-collapse collapse in">
                            <div class="panel-body">Firstly, consult with your family physician.<br>
Secondly, die of the pain and self-pity. Thirdly, somebody else will take your cash home.
</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5" href="#collapse5-2">Do chubby and skinny people have different exercises? </a></h4>
                          </div>
                          <div id="collapse5-2" class="panel-collapse collapse">
                            <div class="panel-body">No, same for everyone.</div>
</div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5" href="#collapse5-3">What If I have health restrictions? </a></h4>
                          </div>
                          <div id="collapse5-3" class="panel-collapse collapse">
                            <div class="panel-body">Please consult with your family physician. You will do a lot of exercising like push-ups, sit-ups, jumping jacks, jogging.
</div>
                          </div>
                        </div>
                        
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5" href="#collapse5-4">Do you make a meal plan?</a></h4>
                          </div>
                          <div id="collapse5-4" class="panel-collapse collapse">
                            <div class="panel-body">No, but I teach you how to make your own meal plan. Using our website you can calculate your daily calories and Fat/Carbs/Protein Balance. I also provide you with the list of prohibited/permitted food &amp; water schedule. No matter who you are: vegetarian, rawatarian or lactating mother, I give you RDI (Recommended Daily Intake) to follow.
</div>
                          </div>
                        </div>
                        
                      </div>
</div>

<!-- END TAB 5 -->

<!-- TAB 6 -->

<div role="tabpanel" class="tab-pane" id="tab6">

 				<div id="accordion6" class="panel-group">
                
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion6" href="#collapse6-1">Who are you, Mr. Mad?</a></h4>
                          </div>
                          <div id="collapse6-1" class="panel-collapse collapse in">
                            <div class="panel-body">I am your frightening dream. If you wanna see my face, you have to win.
</div>
                          </div>
                        </div>
                        
                        
                      </div>
                      
            </div>  

<!-- END TAB 6 -->

<!-- TAB 7 -->

<div role="tabpanel" class="tab-pane" id="tab7">

 				<div id="accordion7" class="panel-group">
                
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion7" href="#collapse7-1">FREE TICKET</a></h4>
                          </div>
                          <div id="collapse7-1" class="panel-collapse collapse in">
                            <div class="panel-body">Are you a blogger ? Do you have more than 50K followers on Instagram? <a href="#contact" class="page-scroll">Contact us</a> and ask how to get a Free&nbsp;Ticket.
</div>
                          </div>
                        </div>
                      </div>
                      
            </div>  

<!-- END TAB 7 -->






         
</div>      
         
</div>      
</div>  
</section> 

<section class="no-bg-color-parallax parallax-black home-section" id="contact" >
 <div class="section-bg" style="background-image:url(/template/img/bg-flares.jpg)"></div>
 
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="heading-wrap animated"  data-animation="bounceInUp" >
        
          <h2 class="section-heading white"> CONTACT US</h2>
       <h3 class="section-subheading hang">Have a question? Read FAQ's twice.<br>
Still have a question? You can ask it here</h3>
    
        </div>
      </div>
    </div>
    <div class="col-lg-12 animated"  data-animation="bounceInUp">
    
    
      <form name="sentMessage"  id="contactForm" novalidate>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
            <div class="form-group">
              <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
              <p class="help-block text-danger"></p>
            </div>
            
          </div>
          <div class="col-md-6">
            <div class="form-group form-group-text">
              <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
              <p class="help-block text-danger"></p>
              <div id="success"></div>
              <button type="submit" class="btn btn-xl  hang">Send Message</button>
            </div>
          </div>
         
        </div>
      </form>
    </div>
  </div>
  <div class="container">
      <div class="row">
       
        <div class="col-xs-12 text-center">
          
          <ul class="social-links">
            <li><a href="https://www.facebook.com/madfatgame/" target="_blank" class="hvr-grow"><i class="fa fa-facebook-square"></i></a></li>
            <li><a href="https://www.instagram.com/madfatgame/" target="_blank"  class="hvr-grow"><i class="fa fa-instagram"></i></a></li>
      
          </ul>
        </div>
       
      </div>
       <div class="section-footer sf-type-2">
      <div class="sf-left" style="background-color:#C00"></div>
      <div class="sf-right"  style="background-color:#C09" ></div>
    </div>
    </div>
</section>


      
    
 

<!-- SECTION -->

<img src="/template/img/fast-food.jpg" width="1280" height="720" class="img-responsive" >

<!--<section class="no-bg-color-parallax home-section"  >
<div class="section-bg" style="background-image:url(img/fast-food.jpg); background-position:bottom center"></div>

 
  <div class="container">
    <div class="row">
      <div class="animated" data-animation="bounceInUp">

            <div class=" col-sm-12 col-md-12 col-lg-12"  >
              
                <h2 class="section-heading white">Still thinking PlAY or not?<br>
My answer — Do not.<br>
It's better to buy<br>
another burger...</h2>
                
              </div>
            </div>
            </div>
            </div>
             <div class="section-footer sf-type-2">
      <div class="sf-left" style="background-color:red"></div>
      <div class="sf-right"  style="background-color:gold" ></div>
    </div>
</section>-->




<!-- SECTION -->


<section id="faux-footer">#MadFatGame &copy; 2016. <a href="https://infoempire.com/" target="_blank">Design, SEO & support by InfoEmpire</a></section>

<!-- SECTION -->






<div class="modal fade" id="signin">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Log In</h4>
            </div>
            <div class="modal-body">
                <p>Need to register an account? <a href="/signup/" title="Register">Create an account</a></p>

                <form id="signin-form" method="post" action="#">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                    </div>
                    <div id="form-reply"></div>
                    <!--<div class="form-group">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"> Keep me logged in
                    </div>-->
                    <div class="form-group">
                        <a href="/login/forgot-username/">Forgot username?</a>
                        <a href="/login/forgot/"> Forgot password?</a>
                    </div>
                    <button id="login-btn" type="submit" class="btn btn-primary btn-lg">LOGIN</button>
                    <button type="submit" class="btn btn-lg btn-secondary">REGISTER</button>
                </form>
            </div>
        </div>
    </div>
</div>






<!--Blazy image--> 
<script type="text/javascript" src="/template/js/blazy.min.js"></script>

<!--Waypoint--> 
<script src="/template/js/waypoints.min.js"></script>

<script src="/template/js/ios-orientationchange-fix.js"></script>

<script src="/template/plugins/bxslider/jquery.bxslider.min.js"></script>

<!--Sly Slider--> 

<!--Bx Slider--> 
<script src="/template/plugins/bxslider/jquery.bxslider.min.js"></script>
<!--Flex Slider--> 
<script src="/template/plugins/flexslider/jquery.flexslider-min.js"></script>

<!-- Catalog Price--> 
<script src="/template/plugins/nouislider/jquery.nouislider.min.js"></script>
<!-- Magnific--> 
<script type="text/javascript" src="/template/plugins/magnific/jquery.magnific-popup.js"></script>
<!-- Pretty Photo --> 
<script src="/template/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
<!-- SMart Menu --> 
<script src="/template/plugins/smart-menu/smart-menu.js"></script>

<script type="text/javascript" src="/template/plugins/sly/sly.min.js"></script>
<!-- Loader --> 
<script src="/template/plugins/loader/js/classie.js"></script>
<script src="/template/plugins/loader/js/pathLoader.js"></script>
<script src="/template/plugins/loader/js/main.js"></script>
<script type="text/javascript" src="/template/js/cssua.min.js"></script>
<script src="/template/plugins/points/points.js" ></script>
<script src="/template/plugins/events/events.js"></script>

<!--<script src="plugins/selectbox/jquery.selectbox-0.2.js"></script> 


<script type="text/javascript" src="plugins/isotope/jquery.isotope.min.js"></script>-->


<script src="/template/js/jqBootstrapValidation.js"></script>
<script src="/template/js/contact_me.js"></script>
<script src="/template/js/jquery.validate.js"></script>
<script src="/template/plugins/events/masonry.pkgd.min.js"></script>


<script src="/template/plugins/newsletter/main.js" ></script>
<script type="text/javascript" src="/template/plugins/bio/js/bio.js"></script>


<script src="/template/plugins/countdown/jquery.countdown.min.js" ></script>



<!--IVIEW--> 
<script src="/template/plugins/iview/js/iview.js"></script>

<!--Scripts--> 
<script type="text/javascript" src="/template/js/scripts.js"></script>

</body>
</html>
