<?php
return array(
    '^account$' => 'account/index',
    '^account/login$' => 'account/index',
    '^account/logout$' => 'account/logOut',
    '^account/ajaxLogin$' => 'account/logIn',
    '^account/profile$' => 'account/profile',
    '^account/anthropometry' => 'account/anthropometry',
    '^account/photos' => 'account/photos',
    '^account/challenges' => 'account/challenges',
    '^account/instastars' => 'account/instastars',
    '^account/chat' => 'account/chat',
    '^profile/check-password$' => 'profile/checkPassword',
    '^profile/save-password$' => 'profile/savePassword',

    //Homepage
    '^index.php$' => 'site/index', // actionIndex in SiteController
    '^$' => 'site/index' // actionIndex in SiteController
);