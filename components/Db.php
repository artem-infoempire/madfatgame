<?php
/**
 * Class db
 */

class Db{
    public $connection;

    public function __construct()
    {
        $argsPath = ROOT . '/config/db_args.php';
        $db_args = include($argsPath);
        $con = new PDO('mysql:host='.$db_args["host"].';dbname=' . $db_args["dbname"], $db_args["user"], $db_args["password"]);

        // Check connection
        if (!$con){
            die('Could not connect to database!');
        } 
        else{
            $this->connection = $con;
        }
    }

}