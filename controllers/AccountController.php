<?php

Class AccountController{

    public function actionIndex(){
        if($id = User::checkLogged()){
            Header("Location: /account/profile");
        }
        else{
            Header("Location: /");
        }
    }

    public function actionLogIn(){
        if(isset($_POST["email"]) && !empty($_POST["email"])) {
            $email = $_POST["email"];
        }
        if(isset($_POST["password"]) && !empty($_POST["password"])) {
            $password = $_POST["password"];
        }

        if(!isset($email) && !isset($password)) {
            echo "Email address or password are incorrect!";
        }
        else{
            //Getting user id
            $id = User::checkUserData($email, $password);
            if (!$id) {
                echo "Email address or password are incorrect!";
            } else {
                echo "success";
            }
        }
    }

    public function actionLogOut(){
        User::logout();
    }

    public function actionProfile(){
        //Checking if user is logged in
        if($id = User::checkLogged()){
            $user = User::getUserById($id);
            $userProfile = User::getUserProfileById($id);
            //Connecting the view
            require_once(ROOT . '/views/account/profile.php');
            return true;
        }
        else{
            Header("Location: /");
        }
    }

    public function actionAnthropometry(){
        //Checking if user is logged in
        if($id = User::checkLogged()){
            $user = User::getUserById($id);
            $userProfile = User::getUserProfileById($id);
            //Connecting the view
            require_once(ROOT . '/views/account/anthropometry.php');
            return true;
        }
        else{
            Header("Location: /");
        }
    }

    public function actionPhotos(){
        //Checking if user is logged in
        if($id = User::checkLogged()){
            $user = User::getUserById($id);
            $userProfile = User::getUserProfileById($id);
            //Connecting the view
            require_once(ROOT . '/views/account/photos.php');
            return true;
        }
        else{
            Header("Location: /");
        }
    }

    public function actionChallenges(){
        //Checking if user is logged in
        if($id = User::checkLogged()){
            $user = User::getUserById($id);
            $userProfile = User::getUserProfileById($id);
            //Connecting the view
            require_once(ROOT . '/views/account/challenges.php');
            return true;
        }
        else{
            Header("Location: /");
        }
    }

    public function actionInstastars(){
        //Checking if user is logged in
        if($id = User::checkLogged()){
            $user = User::getUserById($id);
            $userProfile = User::getUserProfileById($id);
            //Connecting the view
            require_once(ROOT . '/views/account/instastars.php');
            return true;
        }
        else{
            Header("Location: /");
        }
    }

    public function actionChat(){
        //Checking if user is logged in
        if($id = User::checkLogged()){
            $user = User::getUserById($id);
            $userProfile = User::getUserProfileById($id);
            //Connecting the view
            require_once(ROOT . '/views/account/chat.php');
            return true;
        }
        else{
            Header("Location: /");
        }
    }


}