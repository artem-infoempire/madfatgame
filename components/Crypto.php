<?php

Class Crypto{

    public static function crypto_rand($min,$max) {
        $diff = $max - $min;
        if ($diff <= 0) return $min; // not so random...
        $range = $diff + 1; // because $max is inclusive
        $bits = ceil(log(($range),2));
        $bytes = ceil($bits/8.0);
        $bits_max = 1 << $bits;
        // e.g. if $range = 3000 (bin: 101110111000)
        //  +--------+--------+
        //  |....1011|10111000|
        //  +--------+--------+
        //  bits=12, bytes=2, bits_max=2^12=4096
        $num = 0;
        do {
            $num = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes))) % $bits_max;
            if ($num >= $range) {
                continue; // start over instead of accepting bias
            }
            break;
        } while (True);
        return $num + $min;
    }


    // A string comparison function that isn't vulnerable to timing attacks
    // It's not the fastest, but it's more secure against cryptanalysis - Garnet
    public static function crypto_compare($known, $unknown) {
        $ret = true;
        $out = '';

        $loopk = strlen($known);

        $ka = str_split($known);
        $ua = str_split($unknown);

        if(!isset($ua[$loopk-1]) || isset($ua[$loopk]))
        {
            $ret = false;
            $out .= 'Early length detection' . "\n";
        }

        $ub = array();
        $uc = array();
        for($i = 0; $i != $loopk; $i++)
        {
            $uc[$i] = '0';
        }
        for($i = 0; $i != $loopk; $i++)
        {
            if(isset($ua[$i]))
            {
                $ub[$i] = $ua[$i];
            }
            else
            {
                $out .= 'Filler - ' . $i . ' / ' . $loopk . "\n";
                $ub[$i] = $uc[$i];
                $ret = false;
            }
        }
        for($i = 0; $i != $loopk; $i++)
        {
            if($ka[$i] !== $ub[$i])
            {
                $out .= 'Different Character' . "\n";
                $ret = false;
            }
        }
        $out .= 'Same: ' . serialize($ret) . "\n";
        //return $out;
        return $ret;
    }

    public static function makeSalt($num, $validChars = './1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $valid = str_split($validChars);
        $salt = '';
        for($i = 0; $i != $num; $i++)
        {
            $salt .= $valid[self::crypto_rand(0, count($valid) - 1)];
        }
        return $salt;
    }

    //One way Blowfish encryption:
    public static function bcrypt_generate($str, $salt = NULL)
    {
        if(empty($salt))
        {
            $salt = '$2y$' . '12$' . self::makeSalt(22) . '$';
        }
        $bcrypt = crypt($str, $salt);
        return $bcrypt;
    }
    public static function bcrypt_compare($check, $stored)
    {
        //Salt is 22 random chars + 8 known chars, it's therefore the first 30 chars of stored hash
        //$salt = substr($stored, 0, 30);
        //However we can just feed the whole hash in and it will truncate to the salt length
        $compare = self::bcrypt_generate($check, $stored);
        return self::crypto_compare($stored, $compare);
    }


}