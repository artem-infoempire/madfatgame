<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <title>#MadFatGame</title>

    <link rel="shortcut icon" href="http://infoempire.us/madfatgame/img/favicon.png" type="image/png" />
    <link rel="icon" href="http://infoempire.us/madfatgame/img/favicon.png" type="image/png" />
    <!-- Master CSS -->
    <link href="/template/css/master.css" rel="stylesheet">
    <link href="/template/css/custom.css" rel="stylesheet">

    <!-- jQuery-->
    <script src="/template/js/jquery-1.11.1.min.js"></script>
    <script src="/template/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/template/js/jquery-ui.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/template/js/bootstrap.min.js"></script>
    <script src="/template/js/modernizr.js"></script>
    <script src="https://use.fontawesome.com/ef87f083a6.js"></script>


</head>

<!--<body class="animated-all">-->
<body>