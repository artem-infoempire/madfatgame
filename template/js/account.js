function cancelEditPasswordState(){
    $("#current-password").val('');
    $("#new-password").val('');
    $("#repeat-password").val('');
    $("#password-editing").hide();
    $("#password-hidden").show();
    $("#current-password-help").text("");
    $("#new-password-help").text("");
    $("#repeat-password-help").text("");
    $("#current-password").removeClass("border-red").removeClass("border-green");
    $("#new-password").removeClass("border-red").removeClass("border-green");
    $("#repeat-password").removeClass("border-red").removeClass("border-green");
}

function checkNewPassword(){
    var password = $("#new-password").val();
    if(password.length < 8){
        $("#new-password-help").text("Password must be at least 8 characters long!");
        $("#new-password").removeClass("border-green").addClass("border-red");
        return false;
    }
    else{
        $("#new-password-help").text("");
        $("#new-password").removeClass("border-red").addClass("border-green");
        return true;
    }
}

function checkRepeatPassword(){
    var newPassword = $("#new-password").val();
    var repeatPassword = $("#repeat-password").val();
    if(newPassword == repeatPassword){
        $("#repeat-password-help").text("");
        $("#repeat-password").removeClass("border-red").addClass("border-green");
        return true;
    }
    else{
        $("#repeat-password-help").text("Password doesn't match");
        $("#repeat-password").removeClass("border-green").addClass("border-red");
        return false;
    }
}

function savePassword(){
    var currentPassword = $("#current-password").val();
    var newPassword = $("#new-password").val();
    var repeatPassword = $("#repeat-password").val();

    //Checking if new password is longer than 8 characters
    if(!checkNewPassword()){
        return false;
    }
    //Checking if new passwords match
    if(!checkRepeatPassword()){
        return false;
    }
    //Checking if current password is correct
    if(currentPassword != "") {
        $.ajax({
            type: "POST",
            url: '/profile/save-password',
            data: {
                'currentPassword': currentPassword,
                'newPassword': newPassword
            },
            success: function (data) {
                console.log(data);
                if (data == 1) {//if new password has been successfully saved
                    cancelEditPasswordState();
                    $("#save-password-help").text("Password has been successfully saved!").removeClass("text-danger").addClass("text-success");
                }
                else if (data == 2) {//currect password incorrect
                    $("#current-password").removeClass("border-green").addClass("border-red");
                    $("#current-password-help").text("Password is incorrect!");
                }
                else {
                    cancelEditPasswordState();
                    $("#save-password-help").text("There was a problem saving your password!").removeClass("text-success").addClass("text-danger");
                }
            }
        });
    }
    else{
        $("#current-password").removeClass("border-green").addClass("border-red");
        $("#current-password-help").text("Please, enter the password!");
    }

}

$("#edit-password").click(function () {
    $("#password-hidden").hide();
    $("#password-editing").show();
    $("#save-password-help").text("").removeClass("text-danger").removeClass("text-success");
});

$("#cancel-password").click(function(){
    cancelEditPasswordState();
});

$("#save-password").click(function(){
    savePassword();
});

$("#current-password").blur(function(){
    var password = $("#current-password").val();
    $.ajax({
        type: "POST",
        url: '/profile/check-password',
        data: {
            'password' : password
        },
        success: function(data){
            if(data==1){
                $("#current-password-help").text("");
                $("#current-password").removeClass("border-red").addClass("border-green");
            }
            else{
                $("#current-password").removeClass("border-green").addClass("border-red");
                $("#current-password-help").text("Password is incorrect!");
            }
        }
    });
});

$("#new-password").blur(function(){
    checkNewPassword();
});


$("#repeat-password").blur(function(){
    checkRepeatPassword();
});
