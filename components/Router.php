<?php
/**
 * Class router
 * Component for working with the routes
 */
class Router{
    //Attribute to save the routes array
    private $routes;

    //Constructor
    public function __construct()
    {
        //Path to the file with the routes
        $routesPath = ROOT . '/config/routes.php';

        //Getting routes from the file
        $this->routes = include($routesPath);
    }

    //Function that returns URI
    private function getURI(){
        if(!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    //Function for processing requests
    public function run(){
        //getting URI
        $uri = $this->getURI();

        //Checking if this request is in the routes array (routes.php)
        foreach ($this->routes as $uriPattern => $path){
            //Matching $uriPattern and $uri
            if(preg_match("~$uriPattern~", $uri)){
                //Getting an inside route
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                //Determine controller, action, arguments
                $segments = explode('/', $internalRoute);
                $controllerName = array_shift($segments) . "Controller";
                $controllerName = ucfirst($controllerName);

                $actionName = "action" . ucfirst(array_shift($segments));

                $arguments = $segments;

                //connecting Controller class
                $controllerFile = ROOT . "/controllers/" . $controllerName . ".php";

                if(file_exists($controllerFile)){
                    include_once ($controllerFile);
                }
                $controllerObject = new $controllerName;
                
                //Calling the necessary action
                $result = call_user_func_array(array($controllerObject, $actionName), $arguments);
                //if controller's action has been successfully called - stoppign the router
                if ($result != null){
                    break;
                }
            }
        }

    }
}