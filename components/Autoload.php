<?php
/**
* Autoload function for automatic load of classes
*/
function __autoload($class_name){
    $array_paths = array(
        '/models/',
        '/components/',
        '/controllers/'
    );

    //Checking all elements of an array
    foreach($array_paths as $path){
        $path = ROOT . $path . $class_name . ".php";

        //if file exists - we include it
        if(file_exists($path)){
            include_once ($path);
        }
    }
}