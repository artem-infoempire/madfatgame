<?php

Class ProfileController{

    public function actionCheckPassword(){
        //Checking if user is logged in and if password was sent
        if(($id = User::checkLogged()) && ($password = $_POST["password"])){
            echo User::checkPassword($id, $password);
        }
    }
    public function actionSavePassword(){
        //Checking if user is logged in and if password was sent
        if(($id = User::checkLogged()) && ($currentPassword = $_POST["currentPassword"]) && ($newPassword = $_POST["newPassword"])){
            if(!User::checkPassword($id, $currentPassword)){
                echo "2";//2 means password is not correct
            }
            if(!User::updatePassword($id, $newPassword)){
                echo "0";//0 means something went wrong
            }
            else{
                echo "1";//1 means password has been successfully updated
            }
        }
    }
    
}