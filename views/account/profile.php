<?php include_once ROOT . "/views/layouts/header-account.php"; ?>

<section id="account">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div>
                    <img src="/template/img/madfatgame-logo.png" alt="#madfatgame" class="madfat-logo"/>
                    <h3 class="text-center"><?php echo $userProfile["first_name"] . " " . $userProfile["last_name"]; ?></h3>
                </div>
                <div class="list-group">
                    <a href="/account/profile" class="list-group-item active">Profile</a>
                    <a href="/account/anthropometry" class="list-group-item">Anthropometry</a>
                    <a href="/account/photos" class="list-group-item">Photos</a>
                    <a href="/account/challenges" class="list-group-item">Challenges</a>
                    <a href="/account/instastars" class="list-group-item">Instastars</a>
                    <a href="/account/chat" class="list-group-item">Chat</a>
                    <a href="/account/logout" class="list-group-item">Logout</a>
                </div>
            </div>
            <div class="col-sm-10" >

                <h2>Account Info</h2>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>E-mail address</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <strong><?php echo $user["email"]; ?></strong>
                        </p>
                        <p>E-mail is used to authorize into the system</p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right"> 
                        <p>Account password</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <div id="password-hidden" class="group">
                            <p>**********
                                <span id="edit-password" class="edit">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </span>
                            </p>
                            <small id="save-password-help" class="form-text text-muted"></small>
                        </div>
                        <div id="password-editing" class="group">
                            <input type="password" id="current-password" placeholder="Current Password"/>
                            <small id="current-password-help" class="form-text text-muted text-danger"></small>
                            <input type="password" id="new-password" placeholder="New Password"/>
                            <small id="new-password-help" class="form-text text-muted text-danger"></small>
                            <input type="password" id="repeat-password" placeholder="Repeat New Password"/>
                            <small id="repeat-password-help" class="form-text text-muted text-danger"></small>
                            <span id="save-password" class="save">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                            </span>
                            <span id="cancel-password" class="cancel">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right"> 
                        <p>Avatar</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <img src="/template/img/avatar-not-available.png" class="avatar-change-img" alt="<?php echo $userProfile["first_name"] . " " . $userProfile["last_name"]; ?>" />
                            <span id="edit-password"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                        </p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right"> 
                        <p>Measurement System</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <span><strong><?php echo $user["measure"]; ?></strong></span>
                            <span id="edit-password"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                        </p>
                    </div>
                </div>

                <h2>General Info</h2>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right"> 
                        <p>Full Name</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php echo $userProfile["first_name"] . " " . $userProfile["last_name"]; ?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>Birthday</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php
                            echo date_format(date_create($userProfile["birthday"]),"d F Y");
                            ?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>Sex</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php
                                if($userProfile["sex"] == "m"){
                                    echo "Male";
                                }
                                elseif ($userProfile["sex"] == "w"){
                                    echo "Female";
                                }
                            ?>
                        </p>
                    </div>
                </div>

                <h2>Contact Info</h2>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>Phone Number</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php echo $userProfile["phone"]; ?>
                        </p>
                    </div>
                </div>

                <h2>Location</h2>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>Country</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php echo $userProfile["country"]; ?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>City</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php echo $userProfile["city"]; ?>
                        </p>
                    </div>
                </div>

                <h2>Social Media Network Profiles</h2>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>Facebook</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php echo $userProfile["facebook_url"]; ?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6 text-right">
                        <p>Instagram</p>
                    </div>
                    <div class="col-sm-6 text-left">
                        <p>
                            <?php echo $userProfile["instagram_url"]; ?>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include_once ROOT . "/views/layouts/footer-account.php"; ?>
